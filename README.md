# Interfaces Graphiques, Clients lourds et Injection
 ++ Une application fenêtrée avec Swing, des patterns comme MVC, ou encore IOC avec l'injection Spring

## Contexte
* Un premier investisseur s'est intéressé au projet, et il souhaite pouvoir consulter rapidement un prototype sur un appareil quel qu'il soit.
* Nous leur proposons de générer un petit programme s'exécutant sur un de nos PCs pour une démo
* Pour ce faire, nous allons développer un nouveau module, badge-gui, se reposant sur badges-wallet comme dépendance, et y exploiter la technologie Spring...
* Par ailleurs, notre équipe va s'installer dans une ferme d'entreprises où les postes de travail sont tous déjà préconfigurés. Le hic, c'est que l'on a aucun droit pour installer quoi que ce soit, et le seul IDE accessible est IntelliJ en version communautaire. Ceux qui sont en télétravail vont devoir s'aligner sur cet outil en l'installant et en l'utilisant. Ce qui est plutôt plaisant, c'est que cela semble être plutôt efficace, au moins autant qu'éclipse en tout cas...

## Objectifs
* Mises en application:
 - [x] (Exercice 1) Démarrage en 2 temps, affichage rapide de notre liste de métadonnées de Badge, puis customisation fine de la liste ...
    - [x] Commencer avec un tutoriel au choix à afficher simplement la liste des badges (métadonnées), en s'aidant d'une classe d'introspection fournie
    - [x] Implémenter plus sérieusement un modèle de tableau avec des comportements d'affichage des cellules/lignes selon certains critères.   
 - [x] (Exercice 2) Permettre l'affichage d'un badge, lors d'un évènement de double-clic sur une ligne
 - [x] (Exercice 3) Permettre l'ajout d'un badge et la mise à jour visuellement du tableau à l'aide du pattern MVC (Désolé mais Observable/Observer, c'est abandonné depuis Java 9)
 - [x] (Exercice 4) Appliquer le pattern IOC avec Spring pour injecter le DAO, et appliquer ce pattern également sur le projet badges-wallet 

----

## Application de l'IOC et rationnalisation autour des patterns Singleton, Factory, Holder, Delegate, Proxy

 - [ ] Voici ce que doit permettre de mettre en oeuvre l'introduction de classes "Controller" plus spécifiques.

```plantuml
@startuml

title __CONTROLLER's Class Diagram__\n

  namespace fr.cnam.foad.nfa035.badges.gui {
    namespace controller {
      class fr.cnam.foad.nfa035.badges.gui.controller.AddBadgeDialogController {
          + delegateOnOk()
          + validateForm()
      }
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.gui {
    namespace controller {
      class fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController {
          + delegateUIComponentsCreation()
          + delegateUIManagedFieldsCreation()
          + loadBadge()
          + setAddedBadge()
      }
    }
  }
  

  fr.cnam.foad.nfa035.badges.gui.controller.AddBadgeDialogController o-- fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO : dao
  fr.cnam.foad.nfa035.badges.gui.controller.AddBadgeDialogController o-- fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder : displayedBadgeHolder
  fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController o-- fr.cnam.foad.nfa035.badges.gui.components.BadgesPanelFactory : badgesPanelFactory
  fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController o-- fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO : dao
  fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController o-- fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder : displayedBadgeHolder


@enduml

```
 - Vous noterez au passage l'existance d'un **Holder**, en **Singleton**.
 - [ ] Pour passer en Spring-Boot cette application et donc bénéficier du contexte IOC, qui favorise l'application de ce pattern, commençons:
  - [ ] Appliquer cette dépendance dans le fichier **pom.xml** du module badges-gui (mais http://mvnrepository.com aurait pu vous l'apprendre):
  ```xml
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter</artifactId>
         <version>2.5.4</version>
      </dependency>
  ```
  - [ ] Voici ensuite la classe principale de l'application, si l'on veut laisser Spring-Boot prendre le contrôle du projet:
  ```java
   @Configuration
   @SpringBootApplication
   @ComponentScan
   @EnableAutoConfiguration
   public class BadgeWalletApp {
      /**
       * Commentez-moi
       * @param args les arguments
       */
      public static void main(String[] args) {
         ConfigurableApplicationContext ctx = new SpringApplicationBuilder(BadgeWalletApp.class)
                 .headless(false).run(args);
         EventQueue.invokeLater(() -> {
            JFrame frame = new JFrame("My Badge Wallet");
   
            BadgeWalletGUI gui = ctx.getBean(BadgeWalletGUI.class);
   
            frame.setContentPane(gui.getPanelParent());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setIconImage(gui.getIcon().getImage());
            frame.setVisible(true);
         });
      }
   }
  ```
  - [ ] Avec ceci, vos avez la base vous permettant d'utiliser les annotations d'injections:
    - [ ] **@Autowired** pour demander l'instanciation d'une classe candidate en Singleton (par défaut), et la positionner comme membre d'un objet, de façon déclarative (attention, constructeurs vides utilisés)
    - [ ] **@Component(name="facultatif")** pour indiquer qu contexte spring de scanner cet objet comme candida à l'injection.
    - Attention, l'injection se fera par Classe par défaut, ou bien par nom si vous l'avez spécifié (donc nécessité nommage du champ précis en concordance)
    - Voici ce que cela donne sur un exemple:
    ```java
      /**
      * Commentez-moi
      */
      @Component("badgeWallet")
      @Order(value = 2)
      public class BadgeWalletGUI {
    
      private JButton button1;
      private JPanel panelParent;
      private JTable table1;
      private JPanel panelImageContainer;
      private JScrollPane scrollBas;
      private JScrollPane scrollHaut;
      private JPanel panelHaut;
      private JPanel panelBas;
      private BadgePanel badgePanel;
    
    
        private BadgesModel tableModel;
        private List<DigitalBadge> tableList;
    
        @Autowired
        private AddBadgeDialog addBadgeDialog;
    
        @Autowired
        private BadgeWalletController badgesWalletController;
    //....
    ```
    - [ ] A vous de jouer pour simplifier au maximum ce code et le rendre plus fiable et propre grace à l'IOC, le singleton lorsque cela est possible...
      - [ ] Bénéfice 1: réduction du nombre d'arguments dans les méthodes, le singleton appliqué au DAO permettra de l'injecter où l'on veu sans nécesité de l'envoyer dans chaque appel de méthode entre Model/Vue/Controller
      ```plantuml
      @startuml
        
        title __VIEW's Class Diagram__\n
        
        namespace fr.cnam.foad.nfa035.badges.gui {
        namespace view {
        class fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog {
        - buttonCancel : JButton
        - buttonOK : JButton
        - codeSerie : JTextField
        - contentPane : JPanel
        - dateDebut : JXDatePicker
        - dateFin : JXDatePicker
        - fileChooser : JFileChooser
        + AddBadgeDialog()
        + getCodeSerie()
        + getDateDebut()
        + getDateFin()
        + getFileChooser()
        + getIcon()
        {static} + main()
        + postConstruct()
        + propertyChange()
        - onCancel()
        - onOK()
        }
        }
        }
        
        
        namespace fr.cnam.foad.nfa035.badges.gui {
        namespace view {
        class fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI {
        - button1 : JButton
        - panelBas : JPanel
        - panelHaut : JPanel
        - panelImageContainer : JPanel
        - panelParent : JPanel
        - scrollBas : JScrollPane
        - scrollHaut : JScrollPane
        - table1 : JTable
        - tableList : List<DigitalBadge>
        + BadgeWalletGUI()
        + delegateSetAddedBadge()
        + getIcon()
        + getPanelHaut()
        + getPanelImageContainer()
        + getPanelParent()
        + getScrollHaut()
        + getTable1()
        + getTableList()
        + getTableModel()
        + postConstruct()
        + setBadgePanel()
        + setPanelImageContainer()
        + setTableList()
        - createUIComponents()
        - getInstance()
        }
        }
        }
        
        
        namespace fr.cnam.foad.nfa035.badges.gui {
        namespace view {
        class fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder {
        + getDisplayedBadge()
        + setDisplayedBadge()
        }
        }
        }
        
        
        fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog .up.|> java.beans.PropertyChangeListener
        fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog -up-|> javax.swing.JDialog
        fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog o-- fr.cnam.foad.nfa035.badges.gui.controller.AddBadgeDialogController : addBadgeController
        fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog o-- fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI : badgeWallet
        fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog o-- fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder : displayedBadgeHolder
        fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog : addBadgeDialog
        fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.components.BadgePanel : badgePanel
        fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController : badgesWalletController
        fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI o-- fr.cnam.foad.nfa035.badges.gui.model.BadgesModel : tableModel
        fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder o-- fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge : displayedBadge

        
        @enduml

      ```
      - [ ] **Astuce** pour appliquer le Pattern Singleton au DAO, sans toucher à son code qui est dans un autre module, nous pouvons utiliser une Factory:
      ```plantuml
             @startuml

                title __MODEL's Class Diagram__\n
                
                namespace fr.cnam.foad.nfa035.badges.gui {
                namespace model {
                class fr.cnam.foad.nfa035.badges.gui.model.BadgesModel {
                - badges : List<DigitalBadge>
                - entetes : String[]
                {static} - serialVersionUID : long
                + BadgesModel()
                + addBadge()
                + getBadges()
                + getColumnClass()
                + getColumnCount()
                + getColumnName()
                + getRowCount()
                + getValueAt()
                + setBadges()
                }
                }
                }
                
                
                namespace fr.cnam.foad.nfa035.badges.gui {
                namespace model {
                class fr.cnam.foad.nfa035.badges.gui.model.BadgesWalletDAOFactory {
                {static} - RESOURCES_PATH : String
                + getObjectType()
                # createInstance()
                }
                }
                }
                
                
                fr.cnam.foad.nfa035.badges.gui.model.BadgesModel -up-|> javax.swing.table.AbstractTableModel
                fr.cnam.foad.nfa035.badges.gui.model.BadgesWalletDAOFactory -up-|> org.springframework.beans.factory.config.AbstractFactoryBean
                
                
                @enduml
      ```
        - [ ] Et voici le code de cette factory par exemple:
        ```java
        @Component("badgesWalletDAO")
        @Order(value = 1)
        public class BadgesWalletDAOFactory extends AbstractFactoryBean<DirectAccessBadgeWalletDAO> {
            private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";
            @Override
            public Class<?> getObjectType() {
                return DirectAccessBadgeWalletDAOImpl.class;
            }
            @Override
            protected DirectAccessBadgeWalletDAO createInstance() throws IOException {
                return new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            }
        }
        ```
      - [ ] **/!\ Retour à nos composants Swing:** Dans certains cas, l'injection ne peut fonctionner de manière nominale, par exemple lorsque le gestionnaire de formulaire Forms intercepte le contructeur pour accéder à des ressources. Alors comme l'injection par champ annoté ne s'effectue qu'après invocation du constructeur, impossible d'accéder aux ressources injectées au niveau des méthodes d'initialisation liées à Forms puisque forms intercepte le constructeur. 
      - [ ] Mais il est toutefois possible d'accéder au contexte d'injection de façon programmatique:
        ```java
                /**
                * Commentez-moi
                */
                private void createUIComponents() {
                    try {
                        this.badgesWalletController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", BadgeWalletController.class);
            
                         badgesWalletController.delegateUIComponentsCreation(this);
                         badgesWalletController.delegateUIManagedFieldsCreation(this);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
        ```
        - Prenez pour cela cette classe [ApplicationContectProvider](help/badges-gui/src/main/java/fr/cnam/foad/nfa035/badges/gui/context/ApplicationContextProvider.java)

 


[dialog]: screenshots/Dialog_Design.png "Fenêtre de Dialogue pour la création d'un nouveau Badge"
[ihm1]: screenshots/Test-IHM-ok.png
[ihm2]: screenshots/Test-IHM.png

----
