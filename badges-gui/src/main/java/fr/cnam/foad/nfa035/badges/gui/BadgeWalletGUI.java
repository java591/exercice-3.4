package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.*;

@Component("badgeWallet")
@Order(value=2)


/**
 * GUI de l'application
 */
public class BadgeWalletGUI{

    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JPanel panelImageContainer;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelHaut;
    private JPanel panelBas;
    private BadgePanel badgePanel;
    private BadgesModel tableModel;

    public JPanel getPanelHaut() {
        return panelHaut;
    }
    public JScrollPane getScrollHaut(){
        return scrollHaut;
    }

    /**
     * Commentez-moi
     * @return
     */
    public DigitalBadge getBadge() {
        return badge;
    }
    public JPanel getPanelImageContainer(){
        return this.panelImageContainer;
    }
    public void setPanelImageContainer(JPanel panelImageContainer){
        this.panelImageContainer=panelImageContainer;
    }

    private DigitalBadge badge;
    private DirectAccessBadgeWalletDAO dao;
    private List<DigitalBadge> tableList;

    public List<DigitalBadge> getTableList(){
        return tableList;
    }
    public JTable getTable1(){
        return table1;
    }
    public BadgesModel getTableModel(){
        return tableModel;
    }
    public BadgeWalletGUI getInstance(){
        return this;
    }

    @Autowired
    private AddBadgeDialog dialog;
    @Autowired
    private BadgeWalletController badgesWalletController;

    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    /**
     * Constructeur
     * Le constructeur crée et affiche un GUI
     * à l'aide d'un .form qui contient un langage à balises
     * comme le html qui intégre les éléments configuré en Java
     */
    public BadgeWalletGUI() {
        dialog = new AddBadgeDialog();
        dialog.setCaller(this);
        dialog.setDao(dao);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.pack();
                dialog.setLocationRelativeTo(null);
                dialog.setIconImage(dialog.getIcon().getImage());
                dialog.setVisible(true);
            }
        });

        //TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);
        tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize());
        table1.setModel(tableModel);
        table1.setRowSelectionInterval(0, 0);

        // 2. Les Renderers...
        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        // Apparemment, les Objets Number sont traités à part, donc il faut le déclarer explicitement en plus de Object
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        // Idem pour les Dates
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

        // 3. Tri initial
        table1.getRowSorter().toggleSortOrder(0);

        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    try {
                        badgesWalletController.loadBadge(getInstance(),row);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });


    }

    /**
     * Main : affiche le gui du programme
     * @param args
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("My Badge Wallet");
        BadgeWalletGUI gui = new BadgeWalletGUI();
        frame.setContentPane(gui.panelParent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setIconImage(gui.getIcon().getImage());
        frame.setVisible(true);
    }

    public ImageIcon getIcon() {
        return new ImageIcon(getClass().getResource("/logo.png"));
    }

    /*private void createUIComponents() {
        try {
            this.dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            // 1. Le Model
            Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata());
            this.tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            // Badge initial
            this.badge = tableList.get(0);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        this.setCreatedUIFields();

    }*/

    /**
     * Commentez-moi
     */
   /* private void setCreatedUIFields() {
        this.badgePanel = new BadgePanel(badge, dao);
        this.badgePanel.setPreferredSize(new Dimension(256, 256));
        this.panelImageContainer = new JPanel();
        this.panelImageContainer.add(badgePanel);
    }*/


    /**
     * Commentez-moi
     * @param badge
     */
    /*public void setAddedBadge(DigitalBadge badge) {
        this.badge = badge;
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1);
    }*/

    /**
     * Commentez-moi
     * @param row
     */
    /*private void loadBadge(int row){
        panelHaut.removeAll();
        panelHaut.revalidate();

        badge = tableList.get(row);
        setCreatedUIFields();
        table1.setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();
    }*/

    public Container getPanelParent() {
        return null;
    }
    /**
     * création des éléments à afficher depuis la méthode du controller
     */
    private void createUIComponents() {
        try {
            this.badgesWalletController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", BadgeWalletController.class);

            badgesWalletController.delegateUIComponentsCreation(this);
            badgesWalletController.delegateUIManagedFieldsCreation(this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
    public void setTableList(List<DigitalBadge>tableList){
        this.tableList=tableList;
    }

    public void delegateSetAddedBadge(DisplayedBadgeHolder badge) throws IOException {
        badgesWalletController.setAddedBadge(getInstance(),badge);
    }

}
