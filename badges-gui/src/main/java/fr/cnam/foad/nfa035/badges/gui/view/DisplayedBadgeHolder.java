package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("displayedBadgeHolder")
@Order(value=1)
public class DisplayedBadgeHolder {

    private static DisplayedBadgeHolder INSTANCE;

    private DigitalBadge digitalBadge;

    private DisplayedBadgeHolder(){}

    public final static DisplayedBadgeHolder getInstance(){
        if (DisplayedBadgeHolder.INSTANCE == null){
            synchronized (DisplayedBadgeHolder.class) {
                if (DisplayedBadgeHolder.INSTANCE == null)
                    INSTANCE = new DisplayedBadgeHolder();
            }
    }
        return DisplayedBadgeHolder.INSTANCE;
    }

    public void setDisplayedBadge(DigitalBadge digitalBadge) {
        this.digitalBadge = digitalBadge;
    }
    public DigitalBadge getDisplayedBadge(){
        return this.digitalBadge;
    }
}
