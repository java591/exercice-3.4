package fr.cnam.foad.nfa035.badges.gui.components;

import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("badgesPanelFactory")
@Order(value=1)
public class BadgesPanelFactory {
    private static final String RESOURCES_PATH="badges-gui/src/main/resources";
    @Autowired
    DisplayedBadgeHolder displayedBadgeHolder;
    @Autowired
    DirectAccessBadgeWalletDAO dao;

    public BadgePanel getInstance(){
        return new BadgePanel(displayedBadgeHolder.getDisplayedBadge(),dao);
    }

    public BadgePanel getBadgePanel(DigitalBadge badge, DirectAccessBadgeWalletDAO dao){
        return new BadgePanel(badge,dao);
    }
}
