package fr.cnam.foad.nfa035.badges.gui.controller;

import fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.components.BadgesPanelFactory;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * controller du badges wallet
 */
@Component ("badgesWalletController")
public class BadgeWalletController {
    @Autowired
    private BadgesPanelFactory badgesPanelFactory;
    @Autowired
    private DirectAccessBadgeWalletDAO dao;
    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;

    /**
     * creation des éléments et du tableau
     * @param view
     * @throws IOException
     */
    public void delegateUIComponentsCreation(BadgeWalletGUI view)throws IOException {

            // 1. Le Model
            Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata());
            List<DigitalBadge> tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            view.setTableList(tableList);
            // Badge initial
            displayedBadgeHolder.setDisplayedBadge(tableList.get(0));

    }

    /**
     * creation de la mise en place visuel des éléments à afficher
     * @param badgeWalletGUI
     * @throws IOException
     */
    public void delegateUIManagedFieldsCreation(BadgeWalletGUI badgeWalletGUI)throws IOException{
        this.badgesPanelFactory .getBadgePanel(displayedBadgeHolder.getDisplayedBadge(), dao);
        this.badgesPanelFactory.getInstance().setPreferredSize(new Dimension(256, 256));
        badgeWalletGUI.setPanelImageContainer(new JPanel());
        badgeWalletGUI.getPanelImageContainer().add(this.badgesPanelFactory.getInstance());
    }

    /**
     * chargement du badge
     * @param badgeWalletGUI
     * @param row
     * @throws IOException
     */
    public void loadBadge(BadgeWalletGUI badgeWalletGUI,int row) throws IOException {
        badgeWalletGUI.getPanelHaut().removeAll();
        badgeWalletGUI.getPanelHaut().revalidate();

        setAddedBadge(badgeWalletGUI,displayedBadgeHolder);
        delegateUIManagedFieldsCreation(badgeWalletGUI);
        badgeWalletGUI.getTable1().setRowSelectionInterval(row, row);
        badgeWalletGUI.getPanelHaut().add(badgeWalletGUI.getScrollHaut());
        badgeWalletGUI.getPanelImageContainer().setPreferredSize(new Dimension(256, 256));
        badgeWalletGUI.getScrollHaut().setViewportView(badgeWalletGUI.getPanelImageContainer());

        badgeWalletGUI.getPanelHaut().repaint();

    }
    public void setAddedBadge(BadgeWalletGUI badgeWalletGUI,DisplayedBadgeHolder displayedBadgeHolder) throws IOException {
        this.displayedBadgeHolder = displayedBadgeHolder;
        badgeWalletGUI.getTableModel().addBadge(displayedBadgeHolder.getDisplayedBadge());
        badgeWalletGUI.getTableModel().fireTableDataChanged();
        loadBadge(badgeWalletGUI,badgeWalletGUI.getTableModel().getRowCount()-1);
    }
}
